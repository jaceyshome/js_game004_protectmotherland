module.exports = (grunt)->
  fs = require 'fs'
  chalk = require 'chalk'
  startMarker = "[s"
  endMarker = "]"
  dest = null

  readLines = (input, callback)->
    str = ""
    #out = ""
    input.on 'data', (data)->
      str += data
    input.on 'end', ()->
        fixPreTagMarkdown str, callback
        #saveFile out, callback

  fixPreTagMarkdown = (str, callback)->
    split = str.split /\n/
    for key, line of split
      if line.indexOf(startMarker) > 0
        #console.log "marker not at start", line
        startIndex = line.indexOf startMarker
        endIndex = line.indexOf endMarker, startIndex
        newLine = line.substring(startIndex, endIndex+endMarker.length)+line.substring(0, startIndex)+line.substring(endIndex+endMarker.length)
        #console.log "newLine", newLine
        split[key] = newLine
    str = split.join "\n"
    splitTexts str, callback

  splitTexts = (str, callback)->

    texts = {}
    doSplit = ->
      startIndex = str.indexOf startMarker
      if startIndex > -1
        endIndex = str.indexOf endMarker, startIndex
        tagId = str.substring(startIndex + startMarker.length - 1, endIndex)
        str = str.substring endIndex+1
        nextIndex = str.indexOf startMarker
        nextIndex = str.length if nextIndex is -1
        texts[tagId] = (str.substring 0, nextIndex).replace(/^\s+|\s+$/g, '')
        str = str.substring nextIndex
        doSplit()
      else
        outputFiles(texts, callback)
    doSplit()


  outputFiles = (texts, callback)->
    #console.log "outputFiles"
    #console.log @.options
    #console.log @
    #console.log grunt
    fs.mkdir dest, ->
      for k, v of texts
        file = "#{dest}/#{k}.md"
        fs.writeFileSync file, v
        grunt.log.writeln "File #{chalk.cyan(file)} created."
        #file = "helloworld.md"
        #fs.writeFileSync file, "yo"
      callback()

    



  grunt.registerMultiTask 'splitmarkdown', 'Split markdown buy screen text tags.', ->
    done = @async()
    paths = []
    tally = 0
    #console.log @options()
    @files.forEach (file)->
      #console.log file
      dest = file.orig.dest
      file.src.forEach (path)->
        if grunt.file.exists path
          tally++
          paths.push path
    callback = (current_file, current_path)->
      tally--
      if tally is 0
        grunt.log.writeln "Split markdown complete."
        done()
    if paths.length>0
      for path in paths
        input = fs.createReadStream path
        readLines input, callback
    else
      grunt.log.writeln "No markdown files to split."
      done()